﻿#include <iostream>

using namespace std;

class Value{
private:    
  int val;
  int upperBoundary;
  int lowerBoundary;
public:
  Value(int v, int u, int l){
      val = v;
      upperBoundary = u;
      lowerBoundary = l;
  }
  void subtractValue(int s){
      if (val - s < lowerBoundary)
        throw std::exception();
      else
        val -= s;
  }
  void addValue(int a){
      
      if (val + a > upperBoundary)
        throw std::exception();
      else
        val += a;
  }
  void printValue(){
    cout << val << endl;   
  }
};

int main(){
    
    Value *v1 = new Value(5, 10, 0);
    Value *v2 = new Value(9, 100, 0);
    try{
    v1->addValue(15);
    }
    catch (exception e)
    { cout << "Value could exceed limit.\n"; }
    try{
    v2->subtractValue(2000);
    }
    catch (exception e)
    { cout << "Value could exceed limit.\n"; }
    try{
    v2->addValue(15);
    }
    catch (exception e)
    { cout << "Value could exceed limit.\n"; }
    
    v1->printValue();
    v2->printValue();
    return 0;
}