
#include <iostream>
#include <stdio.h>

using namespace std;

class Inventory{
	public:
		Inventory(){
			weapon = "wooden stash";
			armor = "cardboar armor";
		}
		void show_inventory(){			
			printf("Your weapon is %s.\nYour armor is %s.\n", weapon, armor);
		}
		char *weapon;
		char *armor;
};

class Player{
	public:
		Player(){
			gold = 0;
			level = 0;
			inv = new Inventory();
		}
		void show_stats(){
			printf("You have %d gold.\nYou are of level %d.\n", gold, level);
			inv->show_inventory();
		}		
		int gold;
		int level;
		Inventory *inv;
};

int main(){
	
	int controllerInt = 0; 
	bool shopVisited = false;
	Player *player = new Player();
	char *help = "Use ints to control. Print 0 to get Help.\nPrint 73 to show your stats.\nPrint 93 to go to Shop.\nPrint 99 to get Quit.";
	char *hello = "Hey, traveler! You look like you want a quest. You want a hard work, or a simple grind?\nPrint 2 for hard work, print 3 for grind quest.";
	bool continueGame = true;	
	printf("\n%s\n", hello);
	do{		
	switch(controllerInt){
		case 0: printf("%s\n", help);
				break;
		case 2: printf("You go on a Great Adventure and get Great Treasure out of it!\nYou have obtained 1000 gold, you are five levels higher."); 
				player -> gold += 1000;
				player -> level += 5;
				break;
		case 3: player -> gold += 100;
				player -> level += 1;
				printf("You kill goblins for one hour. You gain some xp and 100 gold"); 
				break;
		case 73: player->show_stats(); break;
		case 93: 
			if(shopVisited){
			printf("I`ve got nothing for you now, hero.\n");
			}
			else {
				if (player->gold >= 1000){
				player->gold -= 1000;
				player->inv->weapon = "Charmed Steel Sword of Fire";
				player->inv->armor = "Gold Shining Armor";
				printf("I`m selling you shiny new stuff for 1k gold! Look for yoursef in your inventory by ptinting 73.\n"); 
				shopVisited = true;
				}
				else {
				printf("Looks like you dont have enought money for me. Maybe go do some heroic deeds instead.\n");	
				}
			}
			break;
		case 99: continueGame = false;
				 break;
		default: printf("%s\n", help);
				 break;
		}
	scanf("%d", &controllerInt);	
	} while(continueGame);
	return 0;
}