﻿#include <stdio.h>
using namespace std;

int main(void) {

	int vector[] = { 3, -5, 7, 10, -109, 14, 5, 2, -13 };
	int n = sizeof(vector) / sizeof(vector[0]);
    int *ptr = vector;
    int min = *ptr;
    int *curr;
	for(int i = 1; i < n; i++){
	    if(min > *(ptr + i))
	        min = *(ptr + i);
	}
    printf("%d",min);
	return 0;
}