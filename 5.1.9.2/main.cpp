﻿#include <iostream>
#include <string>

using namespace std;

class Square
{
private:
  double    side;
  double    area;
public:
  Square(double side);
  void set_side(double newSide){
      if(newSide >= 0){
    this->side = newSide;
    this->area = newSide * newSide;}
  }
  void print(Square* square)
    {
    cout << "Square: side=" << square->side << " area=" << square->area << endl;
    }
};

Square::Square(double side)
{
  this->side = side;
  this->area = side * side;
}




int main()
{
  Square s(4);


  s.print(&s);

  s.set_side(2.0);
  s.print(&s);

  s.set_side(-33.0);
  s.print(&s);

  return 0;
}