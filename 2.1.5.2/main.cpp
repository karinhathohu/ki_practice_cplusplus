﻿#include <iostream>

using namespace std;

int main(void) {
	float grossprice, taxrate, netprice, taxvalue;
	
	cout << "Enter a gross price: ";
	cin >> grossprice;
	cout << "Enter a tax rate: ";
	cin >> taxrate;
	
	netprice = grossprice/((100 + taxrate)/100);
	taxvalue = (taxrate)/100 * netprice;
	
	cout << "Net price: " << netprice;
	cout << " Tax value: " << taxvalue << endl;
	
	return 0;
}