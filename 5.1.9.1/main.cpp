﻿#include <iostream>
#include <string>

using namespace std;

class Person
{
public:
  string name;
  int    age;
  string hair;
  string occupation;
  // Your code here
};

void print(Person* person)
{
  cout << person->name << " is " << person->age << " years old" << endl;
  cout << person->name << " has " << person->hair << " hair." << endl;
  cout << person->name << " is a " << person->occupation << endl;
    
}


int main()
{
  Person person;
  person.name = "Harry";
  person.age  = 23;
  person.hair = "black";
  person.occupation = "**Wizard**";
  
  cout << "Meet " << person.name << endl;
  print(&person);

  // Your code here

  return 0;
}