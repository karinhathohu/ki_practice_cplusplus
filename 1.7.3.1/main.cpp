﻿#include <iostream>
#include <stdio.h>

using namespace std;

int main(){
    float a, b, c, d, e;
    a = 2.3;
    b = 2.3;
    c = 2.123456;
    d = 2.123456;
    e = 2.123456;
    
    printf("%.1f\n", a);
    printf("%.2f\n", b);    
    printf("%.6f\n", c); 
    printf("%.2f\n", d); 
    printf("%.0f\n", e); 
    return 0;
}