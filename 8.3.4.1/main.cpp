#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

class Node{
    private:
        int value;
    public:
        Node *leftChild;
        Node *rightChild;
        Node(int v){ 
            value = v;
            leftChild = 0;
            rightChild = 0;
        }
        string Print(){
            string s = "";
    		if (leftChild) {
    		    s += leftChild->Print();
    		}
    		s.append(std::to_string(value));
    		s.append(" ");
    		if (rightChild) {
    		    s += rightChild->Print();
    		}
    		return s;
        }
        bool Add(int v){
            if (!leftChild){
                leftChild = new Node(v);
                return true;
            }
            if (!rightChild){
                 rightChild = new Node(v);
                 return true;
            }
            return false;
        }
};
ostream& operator<<(ostream& os, Node& rootnode)
{
	os << rootnode.Print();
	return os;
}
int main(){
    Node *root = new Node(3);
    root->Add(5);
    root->Add(2);
    
    root->leftChild->Add(11);
    root->leftChild->Add(12);
    root->rightChild->Add(13);
    root->rightChild->Add(14);
    
    root->leftChild->leftChild->Add(15);
    root->leftChild->leftChild->Add(16);
    root->leftChild->rightChild->Add(17);
    root->leftChild->rightChild->Add(18);
    root->rightChild->leftChild->Add(19);
    root->rightChild->leftChild->Add(110);
    root->rightChild->rightChild->Add(111);
    root->rightChild->rightChild->Add(112);
    cout << *root << endl;
    return 0;
}
