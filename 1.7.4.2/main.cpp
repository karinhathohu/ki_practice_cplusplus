﻿#include <iostream>

using namespace std;

int main() {
    float e;
    int i1, i2;
    cin >> i1;
    cin >> i2;
    
    if (1/(float)i1 == 1/(float)i2)
        cout << "Results are equal (by 0.000001 epsilon)";
    else 
        cout << "Results are not equal (by 0.000001 epsilon)";
    return 0;
}